
![Kalibr](https://raw.githubusercontent.com/wiki/ethz-asl/kalibr/images/kalibr_small.png)

 
## Introduction

Kalibr is a toolbox that solves the following calibration problems:

  

1.  **Multiple camera calibration**:

intrinsic and extrinsic calibration of a camera-systems with non-globally shared overlapping fields of view

1.  **Visual-inertial calibration calibration (camera-IMU)**:

spatial and temporal calibration of an IMU w.r.t a camera-system

1.  **Rolling Shutter Camera calibration**:

full intrinsic calibration (projection, distortion and shutter parameters) of rolling shutter cameras

**Please find more information on the [wiki pages](https://github.com/ethz-asl/kalibr/wiki) of this repository.**

## Installation on Ubuntu 20.04

### Installing ROS noetic

Also see the official [ROS Install Page](http://wiki.ros.org/noetic/Installation/Ubuntu) on how to install ROS noetic on Ubuntu 20.04.

Alternative you can look at the [ROS Install From Source Page](http://wiki.ros.org/noetic/Installation/Source) on how to build ROS yourself.

#### Configure your Ubuntu repositories

Configure your Ubuntu repositories to allow "restricted," "universe," and "multiverse." You can follow the Ubuntu guide for instructions on doing this.
#### Setup your sources.list
Setup your computer to accept software from packages.ros.org.
```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
```
[Mirrors](http://wiki.ros.org/ROS/Installation/UbuntuMirrors) [Source Debs](http://wiki.ros.org/DebianPackageSources) are also available

#### Set up your keys
```
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
```
If you experience issues connecting to the keyserver, you can try substituting *hkp://pgp.mit.edu:80* or *hkp://keyserver.ubuntu.com:80* in the previous command.

Alternatively, you can use curl instead of the apt-key command, which can be helpful if you are behind a proxy server:

```
curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xC1CF6E31E6BADE8868B172B4F42ED6FBAB17C654' | sudo apt-key add -
```

#### Installation
First, make sure your Debian package index is up-to-date:
```
sudo apt update
```
Now pick how much of ROS you would like to install.
**Desktop-Full Install: (Recommended):** Everything in Desktop plus 2D/3D simulators and 2D/3D perception packages
```
sudo apt install ros-noetic-desktop-full
```
**Desktop Install:** Everything in ROS-Base plus tools like rqt and rviz
```
sudo apt install ros-noetic-desktop
```
**ROS-Base:** (Bare Bones) ROS packaging, build, and communication libraries. No GUI tools.
```
sudo apt install ros-noetic-ros-base
```

There are even more packages available in ROS. You can always install a specific package directly.
```
sudo apt install ros-noetic-PACKAGE
```
e.g.
```
sudo apt install ros-noetic-slam-gmapping
```
To find available packages, see ROS Index or use:
```
apt search ros-noetic
```

#### Environment setup

You must source this script in every **bash** terminal you use ROS in.
```
source /opt/ros/noetic/setup.bash
```

#### Dependencies for building packages

Up to now you have installed what you need to run the core ROS packages. To create and manage your own ROS workspaces, there are various tools and requirements that are distributed separately. For example, rosinstall is a frequently used command-line tool that enables you to easily download many source trees for ROS packages with one command.

To install this tool and other dependencies for building ROS packages, run:
```
sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
```
##### Initialize rosdep
Before you can use many ROS tools, you will need to initialize rosdep. rosdep enables you to easily install system dependencies for source you want to compile and is required to run some core components in ROS. If you have not yet installed rosdep, do so as follows.
```
sudo apt install python3-rosdep
```
With the following, you can initialize rosdep.
```
sudo rosdep init
rosdep update
```

### Installing bootstrap dependencies

We have used the fork from kalibr from [ori-drs](https://github.com/ori-drs/kalibr.git --branch noetic-devel).
- Create a ROS workspace e.g `~/ros_kalibr`.
- Checkout the Kalibr repository under `~/ros_kalibr/src/kalibr`.
- Use `catkin_init_workspace` to initialize the ROS workspace with Kalibr.
- Use `rosdep` to install almost all required dependencies: `rosdep install --from-paths ./ -iry`.
- Then install the two missing runtime dependencies: `sudo apt install python3-wxgtk4.0 python3-igraph`
- Use `catkin_make_isolated` to build Kalibr, the libraries should be build in `~/ros_kalibr/develop_isolated`.

### Running the Kalibr toolbox

When all the libraries have been build in `~/ros_kalibr/develop_isolated`, use the 'source' command to inialize this
ROS workspace
```
source ~/ros_kalibr/develop_isolated/setup.bash
```

## Tutorial: IMU-camera calibration

A video tutorial for the IMU-camera calibration can be found here:

 
[![alt text](https://user-images.githubusercontent.com/5337083/44033014-50208b8a-9f09-11e8-8e9a-d7d6d3c69d97.png)](https://m.youtube.com/watch?v=puNXsnrYWTY  "imu cam calib")

 
(Credits: @indigomega)

 
## Authors

* Paul Furgale ([email](paul.furgale@mavt.ethz.ch))

* Hannes Sommer ([email](hannes.sommer@mavt.ethz.ch))

* Jérôme Maye ([email](jerome.maye@mavt.ethz.ch))

* Jörn Rehder ([email](joern.rehder@mavt.ethz.ch))

* Thomas Schneider ([email](schneith@ethz.ch))

* Luc Oth

  

## References

The calibration approaches used in Kalibr are based on the following papers. Please cite the appropriate papers when using this toolbox or parts of it in an academic publication.
-  Joern Rehder, Janosch Nikolic, Thomas Schneider, Timo Hinzmann, Roland Siegwart (2016). Extending kalibr: Calibrating the extrinsics of multiple IMUs and of individual axes. In Proceedings of the IEEE International Conference on Robotics and Automation (ICRA), pp. 4304-4311, Stockholm, Sweden.

- Paul Furgale, Joern Rehder, Roland Siegwart (2013). Unified Temporal and Spatial Calibration for Multi-Sensor Systems. In Proceedings of the IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS), Tokyo, Japan.

- Paul Furgale, T D Barfoot, G Sibley (2012). Continuous-Time Batch Estimation Using Temporal Basis Functions. In Proceedings of the IEEE International Conference on Robotics and Automation (ICRA), pp. 2088–2095, St. Paul, MN.

-  J. Maye, P. Furgale, R. Siegwart (2013). Self-supervised Calibration for Robotic Systems, In Proc. of the IEEE Intelligent Vehicles Symposium (IVS)

- L. Oth, P. Furgale, L. Kneip, R. Siegwart (2013). Rolling Shutter Camera Calibration, In Proc. of the IEEE Computer Vision and Pattern Recognition (CVPR)

  

## Acknowledgments

This work is supported in part by the European Union's Seventh Framework Programme (FP7/2007-2013) under grants #269916 (V-Charge), and #610603 (EUROPA2).

  

## License (BSD)

Copyright (c) 2014, Paul Furgale, Jérôme Maye and Jörn Rehder, Autonomous Systems Lab, ETH Zurich, Switzerland<br>

Copyright (c) 2014, Thomas Schneider, Skybotix AG, Switzerland<br>

All rights reserved.<br>

  

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

  

1. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

  

1. All advertising materials mentioning features or use of this software must display the following acknowledgement: This product includes software developed by the Autonomous Systems Lab and Skybotix AG.

  

1. Neither the name of the Autonomous Systems Lab and Skybotix AG nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  

THIS SOFTWARE IS PROVIDED BY THE AUTONOMOUS SYSTEMS LAB AND SKYBOTIX AG ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL the AUTONOMOUS SYSTEMS LAB OR SKYBOTIX AG BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.